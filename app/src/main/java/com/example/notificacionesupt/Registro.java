package com.example.notificacionesupt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.notificacionesupt.Api.Api;
import com.example.notificacionesupt.Api.Servicios.ServicioPeticion;
import com.example.notificacionesupt.ViewModels.Registro_Usuario;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Registro extends AppCompatActivity {
private Button Reg;
private EditText Correo, Password;
private Button btn4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        Reg=(Button)findViewById(R.id.button3);
        Correo=(EditText)findViewById(R.id.editText3);
        Password=(EditText)findViewById(R.id.editText4);
        btn4=(Button)findViewById(R.id.button4);
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent (getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });
        Reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ServicioPeticion service = Api.getApi(Registro.this).create(ServicioPeticion.class);
                Call<Registro_Usuario> registrarCall =  service.registrarUsuario(Correo.getText().toString(),Password.getText().toString());
                registrarCall.enqueue(new Callback<Registro_Usuario>() {
                    @Override
                    public void onResponse(Call<Registro_Usuario> call, Response<Registro_Usuario> response) {
                        Registro_Usuario peticion = response.body();
                        if(response.body() == null){
                            Toast.makeText(Registro.this, "Ocurrio un Error, intentalo más tarde", Toast.LENGTH_LONG).show();
                            return;
                        }
                        if(peticion.estado == "true"){
                            startActivity(new Intent(Registro.this,MainActivity.class));
                            Toast.makeText(Registro.this, "Datos Registrados", Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(Registro.this, peticion.detalle, Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<Registro_Usuario> call, Throwable t) {
                        Toast.makeText(Registro.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}
