package com.example.notificacionesupt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.notificacionesupt.Api.Api;
import com.example.notificacionesupt.Api.Servicios.ServicioPeticion;
import com.example.notificacionesupt.ViewModels.Peticion_Mostrar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Menu extends AppCompatActivity {
    private EditText Not;
    private Button Mostrar, cerrar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        Not = (EditText)findViewById(R.id.editText6);
        Mostrar = (Button) findViewById(R.id.button5);
        cerrar = (Button) findViewById(R.id.button6);

        cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Menu.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        Mostrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ServicioPeticion s = Api.getApi(Menu.this).create(ServicioPeticion.class);
                Call<Peticion_Mostrar> Registros = s.getNoticia();
                Registros.enqueue(new Callback<Peticion_Mostrar>() {
                    @Override
                    public void onResponse(Call<Peticion_Mostrar> call, Response<Peticion_Mostrar> response) {
                        Peticion_Mostrar mostrar = response.body();
                        String Cadena = "";
                        if (mostrar.estado.equals("true")){
                            for (Noti elemento: mostrar.detalle
                            ) {
                                Cadena = Cadena + elemento.id + " | " + elemento.titulo + " | " + elemento.descripcion + " | " + elemento.created_at + " | "  + elemento.updated_at +"\n";
                                Not.setText(Cadena);
                            }




                        }else{
                            Toast.makeText(Menu.this, "Datos Incorrectos", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Peticion_Mostrar> call, Throwable t) {
                        Toast.makeText(Menu.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

    }
}
